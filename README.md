# Conan Server

Docker executing Conan Server

## Run

```bash
docker run --network host -d --restart always localhost:5000/jcamarero/conan_server:1.0
```

or

```bash
docker run -d -p 9300:9300 --restart always localhost:5000/jcamarero/conan_server:1.0
```

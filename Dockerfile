FROM conanio/gcc10:1.32.1

LABEL maintainer="Jorge Camarero Vera"
LABEL maintainer_mail="jcamarerov@gmail.com"

EXPOSE 9300

CMD ["conan_server"]
